#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\MyAutoIt3_Blue.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=ccserp
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#AutoIt3Wrapper_Run_Au3Stripper=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Script Function:
	Расчёты по поисковой выдаче на основе выгрузки из Key Collector v4

	1) Популярность доменов -- расчёт, сколько раз встречается каждый урл и домен
		в выборке скармливаемых фраз;

	2) Расчёт встречаемости заданных доменов против каждой фразы;


	По умолчанию работает первый режим, но если есть файл domains.txt, переключается на 2-й

#ce ----------------------------------------------------------------------------

#Region ; зависимости

#include <MsgBoxConstants.au3>
#include <Array.au3>
#include <File.au3>
#include <Date.au3>
#include "zavisimosti\StringAux.au3"
#include "zavisimosti\GUIAux.au3"
#include "zavisimosti\ExcelAux.au3"
#include "zavisimosti\ArrayRemoveBlanks.au3"
#include "zavisimosti\ArrayWorkshop.au3"

#EndRegion ; зависимости


#Region ; обычные глобальные переменные

Global Const $eMsgBoxTitle = "Расчёт конкурентов по выдаче"

Global Const $eStart = _NowCalc()

#EndRegion ; обычные глобальные переменные


#Region ; класс словаря

Func Dictionary_Construct()

	Return ObjCreate("Scripting.Dictionary")

EndFunc   ;==>Dictionary_Construct


Func _Dictionary_IsValid(ByRef Const $oDict)

	Return ObjName($oDict) == 'Dictionary' ? True : False

EndFunc   ;==>_Dictionary_IsValid


Func Dictionary_SetInner(ByRef $oDict, $sKey)

	If Not _Dictionary_IsValid($oDict) Then Return

	$oDict.Item($sKey) = Dictionary_Construct()

EndFunc   ;==>Dictionary_SetInner

#EndRegion ; класс словаря


#Region ; класс исходных данных

Global Const $eSrcDataGUID = '7c7a8bc2-31dd-4740-aa63-656d696c6493'

Global Enum _
		$eSD_iGUID, _            ; GUID
		$eSD_iUrlsCol, _         ; номер колонки урлов
		$eSD_iTitlesCol, _       ; номер колонки тайтлов
		$eSD_iSnippetsCol, _     ; номер колонки сниппетов (мета дескрипшенов)
		$eSD_iPhrasesCol, _      ; номер колонки фраз
		$eSD_iStartRow, _        ; номер ряда начала данных
		$eSD_aExcel, _           ; массив считанных из эксельки данных
		$eSD_aDomains, _         ; массив доменов
		$eSD_sFilePath, _        ; путь к экселику
		$eSD_iSize               ; размер массива "объекта"

; конструктор

Func SourceD()

	Local $hSourceD[$eSD_iSize]

	$hSourceD[$eSD_iGUID] = $eSrcDataGUID

	_SourceD_SetFilePath($hSourceD)

	If @error Then _MsgBoxFireExit($MB_ICONWARNING, $eMsgBoxTitle, _
			"Не найден файл экспорта поисковой выдачи (ошибка " & @error & ")")

	; по умолчанию предполагается, что файл с доменами отсутствует;
	; если он есть, то потом значение будет изменено

	$hSourceD[$eSD_aDomains] = Null

	$hSourceD[$eSD_iStartRow] = 1 ; строка, с которой начинаются данные (вторая - после строки заголовков)

	$hSourceD[$eSD_iPhrasesCol] = 0

	Return $hSourceD

EndFunc   ;==>SourceD


; здесь и далее -- ф-ции "классов" с префиксом в виде нижнего подчёркивания
; являются "частными", т.е. НЕ предназначены для вызова извне

Func _SourceD_SetFilePath(ByRef $hSourceD)

	Local $hSearch = FileFindFirstFile('* - SERP.xlsx')

	If $hSearch = -1 Then Return SetError(1)

	Local $sFileName = FileFindNextFile($hSearch)

	If Not (@extended = 0) Then Return SetError(2) ; если найдена папка

	FileClose($hSearch)

	$hSourceD[$eSD_sFilePath] = @ScriptDir & '\' & $sFileName

EndFunc   ;==>_SourceD_SetFilePath


; здесь и далее - частные методы _IsObject для проверок, тот ли "объект"

Func SourceD_IsObject(ByRef Const $hSourceD)

	Return UBound($hSourceD) = $eSD_iSize And _
			$hSourceD[$eSD_iGUID] == $eSrcDataGUID

EndFunc   ;==>SourceD_IsObject


; массивы (и производные значения) устанавливаются отдельно, чтобы избежать
; возврата массива с массивами (большие данные) из конструктора

Func SourceD_SetArrays(ByRef $hSourceD)

	If Not SourceD_IsObject($hSourceD) Then Return

	Local $aArray

	; если лист не 1й, то не работает (?)

	_ReadExcelToArr($hSourceD[$eSD_sFilePath], 1, $aArray)

	If @error Then _MsgBoxFireExit($MB_ICONERROR, $eMsgBoxTitle, _
			"Не получилось прочитать эксельку " _
			 & $hSourceD[$eSD_sFilePath] & " (ошибка " & @error & ")")

	$hSourceD[$eSD_sFilePath] = 0 ; более нигде не понадобится, нет смысла занимать память

	$hSourceD[$eSD_aExcel] = $aArray

	$aArray = 0

	_SourceD_SetFloatCols($hSourceD)

	If @error Then _MsgBoxFireExit($MB_ICONWARNING, $eMsgBoxTitle, _
			"В эксельке не найдена колонка url'ов")

	_SourceD_SetDomains($hSourceD)

EndFunc   ;==>SourceD_SetArrays


; установка номеров колонок: урлы, тайтлы, сниппеты

Func _SourceD_SetFloatCols(ByRef $hSourceD)

	$hSourceD[$eSD_iUrlsCol] = Null
	$hSourceD[$eSD_iTitlesCol] = Null
	$hSourceD[$eSD_iSnippetsCol] = Null

	Local $sRegexpContinue = "\s\[" & "(Yandex|Google|Mail\.ru|YouTube)" & "\]$"

	Local $sUrlRegexp = "^URL" & $sRegexpContinue
	Local $sTitleRegexp = "^Title" & $sRegexpContinue
	Local $sSnippetRegexp = "^Snippet" & $sRegexpContinue

	Local $aArray = $hSourceD[$eSD_aExcel]

	For $i = 1 To UBound($aArray, 2) - 1 ; обходим колонки; первый (0-й) столбец - фраза, пропускаем

		; на первой строке ищем заголовки
		; колонок url'ов, тайтлов и сниппетов

		If Not $hSourceD[$eSD_iUrlsCol] And _
				StringRegExp($aArray[0][$i], $sUrlRegexp) Then

			$hSourceD[$eSD_iUrlsCol] = $i

			ContinueLoop
		EndIf

		If Not $hSourceD[$eSD_iTitlesCol] And _
				StringRegExp($aArray[0][$i], $sTitleRegexp) Then

			$hSourceD[$eSD_iTitlesCol] = $i

			ContinueLoop
		EndIf

		If Not $hSourceD[$eSD_iSnippetsCol] And _
				StringRegExp($aArray[0][$i], $sSnippetRegexp) Then

			$hSourceD[$eSD_iSnippetsCol] = $i

			ContinueLoop
		EndIf

		; нет досрочного прерывания цикла потому что искомые
		; колонки располагаются в самом конце, как правило
	Next

	; обязательной является только колонка урлов

	If Not $hSourceD[$eSD_iUrlsCol] Then Return SetError(1)

EndFunc   ;==>_SourceD_SetFloatCols


; установка доменов из файла доменов

Func _SourceD_SetDomains(ByRef $hSourceD)

	Local $sFilePath = @ScriptDir & '\domains.txt'

	; здесь и далее достаточно обычного возврата, т.к. данные
	; уже установлены в нужном виде, и прерывание с ошибкой не надо

	If Not FileExists($sFilePath) Then Return

	_SourceD_SetDomains_Read($hSourceD, $sFilePath)

	If @extended Then
		Return
	ElseIf @error Then
		_MsgBoxFireExit($MB_ICONERROR, $eMsgBoxTitle, _
				"Не удалось прочитать список доменов (ошибка " & @error & ")")
	EndIf

	_SourceD_SetDomains_ToLower($hSourceD)

	If @error Then _MsgBoxFireExit($MB_ICONWARNING, $eMsgBoxTitle, _
			"В содержимом файла доменов не найдено ничего похожего на домен")

	_SourceD_SetDomains_DelBlanks($hSourceD)

	If @extended Then
		Return
	ElseIf @error Then
		_MsgBoxFireExit($MB_ICONERROR, $eMsgBoxTitle, _
				"Не удалось почистить список доменов (ошибка " & @error & ")")
	EndIf
EndFunc   ;==>_SourceD_SetDomains


; считывание доменов из файла

Func _SourceD_SetDomains_Read(ByRef $hSourceD, $sFilePath)

	Local $aArray

	Local $vRet = _FileReadToArray($sFilePath, $aArray)

	Select
		Case $vRet = 1

			$hSourceD[$eSD_aDomains] = $aArray

		Case @error = 2

			; файл пустой

			Return SetError(0, 1)

		Case Else
			; выдать ошибку затем, что раз файл присутствует, то дальнейшая работа строится
			; в зависимости от его содержимого, а невозможность прочитать текстовый файл
			; свидетельствует о том, что явно что-то не так

			Return SetError(@error)

	EndSelect
EndFunc   ;==>_SourceD_SetDomains_Read


; привести каждый домен к нижнему регистру (для последующих
; корректных сравнений) и поверхностно проверить, содержит ли
; файл нужные данные

Func _SourceD_SetDomains_ToLower(ByRef $hSourceD)

	Local $bValid

	Local $aArray = $hSourceD[$eSD_aDomains] ; будем перезаписывать значения в массиве, поэтому нельзя напрямую использовать массив в "объекте"

	For $i = 1 To $aArray[0]

		; сразу приводим всё к нижнему регистру;
		; даже если файл окажется недействительным, всё равно
		; это целесообразнее, чем обходить массив дважды

		$aArray[$i] = StringLower($aArray[$i])

		; искать первое значение, которое похоже на домен

		If Not $bValid And StringInStr($aArray[$i], ".") Then $bValid = True
	Next

	; если не найдено ни одно значение с точкой -- содержимое файла
	; врядли пригодно для пробива доменов против фраз;
	; но так как юзер возможно рассчитывает получить
	; результат по фразам/доменам (раз файл не пустой),
	; то прерываем работу

	If Not $bValid Then Return SetError(1)

	$hSourceD[$eSD_aDomains] = $aArray

EndFunc   ;==>_SourceD_SetDomains_ToLower


; удалить пустые строки в массиве доменов

Func _SourceD_SetDomains_DelBlanks(ByRef $hSourceD)

	Local $aArray = $hSourceD[$eSD_aDomains]

	_ArrayRemoveBlanks($aArray, Default, Default, True)

	If @error Then Return SetError(@error)

	If $aArray[0] <> 0 Then

		$hSourceD[$eSD_aDomains] = $aArray

	Else
		; если все строки будут удалены из-за того, что пустые,
		; то счётчик станет равен 0

		$hSourceD[$eSD_aDomains] = Null

		Return SetError(0, 1)
	EndIf
EndFunc   ;==>_SourceD_SetDomains_DelBlanks

#EndRegion ; класс исходных данных


#Region ; класс выходных данных

Global Const $eResDataGUID = '0b5427e6-990c-46fc-88d1-61b6355ac4ef'

Global Enum _
		$eRD_iGUID, _                ; GUID
		$eRD_iRow, _                 ; номер текущего ряда
		$eRD_iTitlesCol, _           ; номер колонки тайтлов в массиве доменов
		$eRD_iSnippetsCol, _         ; номер колонки сниппетов в массиве доменов
		$eRD_iColUbound, _           ; число колонок в массиве доменов
		$eRD_iRowUbound, _           ; число строк в массиве доменов
		$eRD_iPhrasesCounter, _      ; счётчик фраз
		$eRD_sDomain, _              ; домен
		$eRD_sUrl, _                 ; урл
		$eRD_sPhrase, _              ; фраза
		$eRD_oDomains, _             ; словарь доменов
		$eRD_oExtDomains, _          ; словарь доменов с доп. данными
		$eRD_oUrls, _                ; словарь урлов для домена
		$eRD_oRowsUrls, _            ; словарь урлов с номерами строк в значениях
		$eRD_oPhraseUrlsDomains, _   ; словарь доменов (и урлов) для одной фразы
		$eRD_oPhrases, _             ; словарь фраз
		$eRD_aDomains, _             ; массив доменов
		$eRD_aUrls, _                ; массив урлов
		$eRD_aPhrases, _             ; массив фраз
		$eRD_iSize                   ; размер массива

; конструктор

Func ResD()

	Local $hResD[$eRD_iSize]

	$hResD[$eRD_iGUID] = $eResDataGUID

	$hResD[$eRD_iTitlesCol] = Null
	$hResD[$eRD_iSnippetsCol] = Null

	Return $hResD

EndFunc   ;==>ResD


Func ResD_IsObject(ByRef Const $hResD)

	Return UBound($hResD) = $eRD_iSize And _
			$hResD[$eRD_iGUID] == $eResDataGUID

EndFunc   ;==>ResD_IsObject


; для установки произвольного значения свойства "объекта" по индексу массива "объекта";
; нежелеательно использовать для назначения массивов/объектов -- чтобы не передавать их
; в эту ф-цию (нет ByRef для значения); назначение ф-ции - более явно выделить в коде
; присвоение значений в объекте - там, где это полезно

Func _ResD_SetValue(ByRef $hResD, $iIndex, $vVal)

	$hResD[$iIndex] = $vVal

EndFunc   ;==>_ResD_SetValue


; для установки урла; текущий ряд и колонка урлов должны быть заданы в соотв-х "объектах"

Func _ResD_SetUrl(ByRef $hResD, ByRef Const $hSourceD)

	Local $sUrl = ($hSourceD[$eSD_aExcel])[$hResD[$eRD_iRow]][$hSourceD[$eSD_iUrlsCol]]

	If $sUrl Then

		$hResD[$eRD_sUrl] = $sUrl
	Else
		; если по какой-то причине ячейка с урлом пустая

		Return SetError(1)
	EndIf
EndFunc   ;==>_ResD_SetUrl


; для установки домена из урла; по умолчанию без изменения регистра

Func _ResD_SetDomain(ByRef $hResD, $bToLower = False)

	$hResD[$eRD_sDomain] = _GetDomainFromUrl($hResD[$eRD_sUrl])

	If $bToLower Then _
			$hResD[$eRD_sDomain] = StringLower($hResD[$eRD_sDomain])

EndFunc   ;==>_ResD_SetDomain


; проверка строки на пустоту

Func _ResD_IsEmptyRow(ByRef Const $hResD, ByRef Const $hSourceD)

	; если пусто, то это строка-разделитель данных разных фраз

	If ($hSourceD[$eSD_aExcel])[$hResD[$eRD_iRow]][$hSourceD[$eSD_iPhrasesCol]] Then

		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>_ResD_IsEmptyRow


; проверка отличия текущей фразы от той, которая в предыдущем ряду

Func _ResD_IsPhraseDiffers(ByRef Const $hResD, ByRef Const $hSourceD)

	Local $iRow = $hResD[$eRD_iRow]
	Local $iCol = $hSourceD[$eSD_iPhrasesCol]

	; если ячейка равна предыдущей, то фраза та же

	If ($hSourceD[$eSD_aExcel])[$iRow][$iCol] = _
			($hSourceD[$eSD_aExcel])[$iRow - 1][$iCol] Then

		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>_ResD_IsPhraseDiffers


; реинициализация словаря по индексному номеру массива,
; где располагается словарь

Func _ResD_DictReInit(ByRef $hResD, $i)

	$hResD[$i] = Dictionary_Construct()

EndFunc   ;==>_ResD_DictReInit


; добавить запись по индексу в словарь по индексу

Func _ResD_DictAddRecord(ByRef $hResD, $ioIndex, $isIndex, $vVal = 0)

	If Not $hResD[$ioIndex].Exists($hResD[$isIndex]) Then

		$hResD[$ioIndex].Add($hResD[$isIndex], $vVal)
	Else
		Return SetError(1)
	EndIf
EndFunc   ;==>_ResD_DictAddRecord


; обновление счётчика для словарей, в которых ведётся подсчёт встречаемости сущностей

Func _ResD_DictCounterUpdate(ByRef $hResD, $iDictIndex, $iKeyIndex)

	Local $sKey = $hResD[$iKeyIndex]

	If Not $hResD[$iDictIndex].Exists($sKey) Then

		$hResD[$iDictIndex].Add($sKey, 1) ; указываем начальную встречаемость

	Else
		$hResD[$iDictIndex].Item($sKey) = $hResD[$iDictIndex].Item($sKey) + 1 ; увеличиваем счётчик встречаемости

	EndIf
EndFunc   ;==>_ResD_DictCounterUpdate


; перевод словаря в простой двухколоночный массив

Func _ResD_DictToArray(ByRef $hResD, $ioIndex, $iaIndex, $bAddCounter = False)

	$hResD[$iaIndex] = $hResD[$ioIndex].Keys

	Local $aTemp = $hResD[$ioIndex].Items

	_ArrayAttach($hResD[$iaIndex], $aTemp, 2)

	$aTemp = 0

	If $bAddCounter Then _ArrayInsert($hResD[$iaIndex], 0, $hResD[$ioIndex].Count)

	$hResD[$ioIndex] = 0 ; после наполнения массива словарь не нужен

EndFunc   ;==>_ResD_DictToArray


; для сортировки простого двухколоночного массива по убыванию
; значений во второй (1-й) колонке

Func _ResD_ArraySortDesc(ByRef $hResD, $iIndex, $iStart = 0)

	_ArraySort($hResD[$iIndex], 1, $iStart, Default, 1)

EndFunc   ;==>_ResD_ArraySortDesc


; добавить в простой двухколоночный массив строку заголовков, либо заменить счётчики заголовками

Func _ResD_ArrayInsertTwoHeads(ByRef $hResD, $iIndex, $sHead1, $sHead2, $bHasCounter = False)

	Local $aArray

	If Not $bHasCounter Then

		_ArrayInsert($hResD[$iIndex], 0, $sHead1 & "|" & $sHead2)
	Else
		$aArray = $hResD[$iIndex]

		$aArray[0][0] = $sHead1
		$aArray[0][1] = $sHead2

		$hResD[$iIndex] = $aArray
	EndIf
EndFunc   ;==>_ResD_ArrayInsertTwoHeads


Func ResD_CountDomainsPopularity_CDP(ByRef $hResD, ByRef Const $hSourceD)

	If Not ResD_IsObject($hResD) _
			Or Not SourceD_IsObject($hSourceD) Then Return

	_ResD_CDP_Core($hResD, $hSourceD)

	_ResD_CDP_SetArrayDomains_SAD($hResD, $hSourceD)

	; если потом будет внедрена короткая работа (данные по доменам без урлов):
	;_ResD_DictToArray($hResD, $eRD_oDomains, $eRD_aDomains)
	;_ResD_ArraySortDesc($hResD, $eRD_aDomains)

	Local $sTitleOccurence = "Встречается, раз (из " _
			 & $hResD[$eRD_iPhrasesCounter] & ")"

	_ResD_CDP_ArrayDomainsSetHeads($hResD, $hSourceD, $sTitleOccurence)

	; если потом будет внедрена короткая работа:
	;_ResD_ArrayInsertTwoHeads($hResD, $eRD_aDomains, "Домен", $sTitleOccurence)

EndFunc   ;==>ResD_CountDomainsPopularity_CDP


Func _ResD_CDP_Core(ByRef $hResD, ByRef Const $hSourceD)

	; в словаре временных записей будет вестись учёт доменов
	; и урлов для одной фразы, чтобы домен или урл,
	; попавшийся для фразы, учитывался только один раз
	; (на тот случай, если встречается для фразы более одного раза);
	; для доменов такая проверка обязательна, а для урлов - полезна
	; (хотя наличие двух и более одинаковых урлов в рамках одной фразы
	; редко встречается)

	_ResD_SetValue($hResD, $eRD_iPhrasesCounter, 0) ; нужно только для вывода в результатах общего числа фраз

	_ResD_DictReInit($hResD, $eRD_oDomains)
	_ResD_DictReInit($hResD, $eRD_oExtDomains)
	_ResD_DictReInit($hResD, $eRD_oRowsUrls)

	For $i = $hSourceD[$eSD_iStartRow] _
			To UBound($hSourceD[$eSD_aExcel]) - 1 ; обходим все урлы

		_ResD_SetValue($hResD, $eRD_iRow, $i)

		; экспорт КК4 может быть двух видов:
		; 1) фразы идут сплошняком;
		; 2) разделитель групп фраз -- пустая строка;
		; это для обработки варианта 2

		If _ResD_IsEmptyRow($hResD, $hSourceD) Then ContinueLoop

		If _ResD_IsPhraseDiffers($hResD, $hSourceD) Then

			$hResD[$eRD_iPhrasesCounter] += 1

			_ResD_DictReInit($hResD, $eRD_oPhraseUrlsDomains)
		EndIf

		_ResD_SetUrl($hResD, $hSourceD)

		If @error Then ContinueLoop

		; наполнение словаря урлов со значениями в виде номеров строк эксели, где встречаются эти урлы

		_ResD_DictAddRecord($hResD, $eRD_oRowsUrls, $eRD_sUrl, $i)

		_ResD_SetDomain($hResD)

		_ResD_CDP_CountDomainAndUrl($hResD)
	Next

	; здесь и в похожих случаях обнуления словарей/массивов "объекта" --
	; данные более не нужны, освобождаем память

	$hResD[$eRD_oPhraseUrlsDomains] = 0

EndFunc   ;==>_ResD_CDP_Core


; для обновления счётчиков урлов и доменов, и наполнения словарей урлов для каждого домена

Func _ResD_CDP_CountDomainAndUrl(ByRef $hResD)

	_ResD_DictAddRecord($hResD, $eRD_oPhraseUrlsDomains, $eRD_sUrl)

	; если урл уже встречался и обрабатывался для текущей фразы,
	; то значит, и домен, и урл для домена

	If @error Then Return

	_ResD_DictAddRecord($hResD, $eRD_oPhraseUrlsDomains, $eRD_sDomain)

	If Not @error Then

		_ResD_DictCounterUpdate($hResD, $eRD_oDomains, $eRD_sDomain)

		_ResD_CDP_DomainUrlsInit($hResD)

	EndIf

	_ResD_CDP_DomainUrlsUpdate($hResD)

EndFunc   ;==>_ResD_CDP_CountDomainAndUrl


; создать словарь для учёта урлов домена

Func _ResD_CDP_DomainUrlsInit(ByRef $hResD)

	If $hResD[$eRD_oExtDomains].Exists($hResD[$eRD_sDomain]) Then Return

	Dictionary_SetInner($hResD[$eRD_oExtDomains], $hResD[$eRD_sDomain])

EndFunc   ;==>_ResD_CDP_DomainUrlsInit


; обновление словаря урлов домена

Func _ResD_CDP_DomainUrlsUpdate(ByRef $hResD)

	_ResD_CDP_DomainUrlsSet($hResD)

	_ResD_DictCounterUpdate($hResD, $eRD_oUrls, $eRD_sUrl)

	$hResD[$eRD_oExtDomains].Item($hResD[$eRD_sDomain]) = $hResD[$eRD_oUrls]

EndFunc   ;==>_ResD_CDP_DomainUrlsUpdate


; для установки словаря урлов домена в "объекте"

Func _ResD_CDP_DomainUrlsSet(ByRef $hResD)

	$hResD[$eRD_oUrls] = $hResD[$eRD_oExtDomains].Item($hResD[$eRD_sDomain])

EndFunc   ;==>_ResD_CDP_DomainUrlsSet


; перевод словарей доменов в один массив

Func _ResD_CDP_SetArrayDomains_SAD(ByRef $hResD, ByRef Const $hSourceD)

	_ResD_DictToArray($hResD, $eRD_oDomains, $eRD_aDomains, True)

	_ResD_ArraySortDesc($hResD, $eRD_aDomains, 1)

	_ResD_CDP_SAD_SetRowsAmount($hResD)

	_ResD_CDP_SAD_SetCols($hResD, $hSourceD)

	Local $aDomains[$hResD[$eRD_iRowUbound]][$hResD[$eRD_iColUbound]] ; массив доменов и урлов (итоговый)

	_ResD_SetValue($hResD, $eRD_iRow, -1) ; работа будет с 0-го ряда (у итогового массива нет счётчика), а увеличение счётчика ряда будет в начале цикла

	For $i = 1 To ($hResD[$eRD_aDomains])[0][0]

		_ResD_SetValue($hResD, $eRD_iRow, $hResD[$eRD_iRow] + 1)

		$aDomains[$hResD[$eRD_iRow]][0] = ($hResD[$eRD_aDomains])[$i][0]

		$aDomains[$hResD[$eRD_iRow]][1] = ($hResD[$eRD_aDomains])[$i][1]

		_ResD_SetValue($hResD, $eRD_sDomain, ($hResD[$eRD_aDomains])[$i][0])

		_ResD_CDP_SAD_AddExtData($aDomains, $hResD, $hSourceD)
	Next

	$hResD[$eRD_oExtDomains] = 0
	$hResD[$eRD_oRowsUrls] = 0
	$hResD[$eRD_aUrls] = 0

	$hResD[$eRD_aDomains] = $aDomains

EndFunc   ;==>_ResD_CDP_SetArrayDomains_SAD


; вычисление количества строк итогового массива доменов и урлов

Func _ResD_CDP_SAD_SetRowsAmount(ByRef $hResD)

	Local $iRows = 0

	For $sDomain In $hResD[$eRD_oExtDomains]

		$iRows += $hResD[$eRD_oExtDomains].Item($sDomain).Count
	Next

	$hResD[$eRD_iRowUbound] = $iRows

EndFunc   ;==>_ResD_CDP_SAD_SetRowsAmount


; получить число колонок для массива доменов

Func _ResD_CDP_SAD_SetCols(ByRef $hResD, ByRef Const $hSourceD)

	$hResD[$eRD_iColUbound] = 4 ; колонки: домен, встречается раз, урл, встречается

	If $hSourceD[$eSD_iTitlesCol] Then

		$hResD[$eRD_iColUbound] += 1
		$hResD[$eRD_iTitlesCol] = 4

		If $hSourceD[$eSD_iSnippetsCol] Then

			$hResD[$eRD_iColUbound] += 1
			$hResD[$eRD_iSnippetsCol] = 5
		EndIf
	EndIf
EndFunc   ;==>_ResD_CDP_SAD_SetCols


; добавить данные в массив доменов -- из доп. данных по доменам

Func _ResD_CDP_SAD_AddExtData(ByRef $aDomains, ByRef $hResD, ByRef Const $hSourceD)

	_ResD_CDP_DomainUrlsSet($hResD)

	_ResD_DictToArray($hResD, $eRD_oUrls, $eRD_aUrls, True)

	_ResD_ArraySortDesc($hResD, $eRD_aUrls, 1)

	_ResD_SetValue($hResD, $eRD_iRow, $hResD[$eRD_iRow] - 1) ; корректируем, чтобы далее начать с того ряда, где расположен домен

	For $i = 1 To ($hResD[$eRD_aUrls])[0][0]

		_ResD_SetValue($hResD, $eRD_iRow, $hResD[$eRD_iRow] + 1)

		$aDomains[$hResD[$eRD_iRow]][0] = $hResD[$eRD_sDomain] ; домены в первой (0-й) колонке

		$aDomains[$hResD[$eRD_iRow]][2] = ($hResD[$eRD_aUrls])[$i][0] ; урлы в третьей (2-й)

		$aDomains[$hResD[$eRD_iRow]][3] = ($hResD[$eRD_aUrls])[$i][1] ; встречаемость урлов

		_ResD_SetValue($hResD, $eRD_sUrl, ($hResD[$eRD_aUrls])[$i][0])

		_ResD_CDP_SAD_AddTitlesSnippets($aDomains, $hResD, $hSourceD)
	Next
EndFunc   ;==>_ResD_CDP_SAD_AddExtData


; для добавления тайтлов и сниппетов в массив доменов;
; передача массива доменов byref (а не в "объекте") потому что иначе сильно замедляется
; выполнение -- переназначение массива доменов лишних 2-3 раза
; за итерацию в цикле

Func _ResD_CDP_SAD_AddTitlesSnippets(ByRef $aDomains, _
		ByRef Const $hResD, ByRef Const $hSourceD)

	; в словаре $hResD[$eRD_oRowsUrls] содержатся данные по номерам строк в массиве эксели,
	; где располагаются урлы - оттуда же берём и данные из колонок тайтлов и сниппетов

	If $hResD[$eRD_iTitlesCol] Then _
			$aDomains[$hResD[$eRD_iRow]][$hResD[$eRD_iTitlesCol]] = _
			($hSourceD[$eSD_aExcel])[$hResD[$eRD_oRowsUrls].Item($hResD[$eRD_sUrl])][$hSourceD[$eSD_iTitlesCol]]

	If $hResD[$eRD_iSnippetsCol] Then _
			$aDomains[$hResD[$eRD_iRow]][$hResD[$eRD_iSnippetsCol]] = _
			($hSourceD[$eSD_aExcel])[$hResD[$eRD_oRowsUrls].Item($hResD[$eRD_sUrl])][$hSourceD[$eSD_iSnippetsCol]]

EndFunc   ;==>_ResD_CDP_SAD_AddTitlesSnippets


; для указания заголовков в массиве доменов

Func _ResD_CDP_ArrayDomainsSetHeads(ByRef $hResD, _
		ByRef Const $hSourceD, $sTitleOccurence)

	Local $aArray = $hResD[$eRD_aDomains]

	_ArrayInsert($aArray, 0) ; пустая строка для заголовков

	$aArray[0][0] = "Домен"

	$aArray[0][1] = $sTitleOccurence

	$aArray[0][2] = "URL"

	$aArray[0][3] = "Встречается раз"

	If $hSourceD[$eSD_iTitlesCol] Then

		$aArray[0][4] = "Title"

		If $hSourceD[$eSD_iSnippetsCol] Then $aArray[0][5] = "Snippet"
	EndIf

	$hResD[$eRD_aDomains] = $aArray

EndFunc   ;==>_ResD_CDP_ArrayDomainsSetHeads


Func ResD_CountPhrasesPopularity_CPP(ByRef $hResD, ByRef Const $hSourceD)

	If Not ResD_IsObject($hResD) _
			Or Not SourceD_IsObject($hSourceD) Then Return

	_ResD_DictReInit($hResD, $eRD_oDomains)

	For $i = 1 To ($hSourceD[$eSD_aDomains])[0]

		_ResD_SetValue($hResD, $eRD_sDomain, ($hSourceD[$eSD_aDomains])[$i])

		_ResD_DictAddRecord($hResD, $eRD_oDomains, $eRD_sDomain)
	Next

	_ResD_CPP_Core($hResD, $hSourceD)

	$hResD[$eRD_oDomains] = 0
	$hResD[$eRD_oPhraseUrlsDomains] = 0

	_ResD_DictToArray($hResD, $eRD_oPhrases, $eRD_aPhrases)

	_ResD_ArrayInsertTwoHeads($hResD, $eRD_aPhrases, _
			"Фраза", "Кол-во заданных доменов в ТОПе")

EndFunc   ;==>ResD_CountPhrasesPopularity_CPP


Func _ResD_CPP_Core(ByRef $hResD, ByRef Const $hSourceD)

	_ResD_DictReInit($hResD, $eRD_oPhrases)

	; обходим все урлы

	For $i = $hSourceD[$eSD_iStartRow] _
			To UBound($hSourceD[$eSD_aExcel]) - 1

		_ResD_SetValue($hResD, $eRD_iRow, $i)

		If _ResD_IsEmptyRow($hResD, $hSourceD) Then ContinueLoop

		If _ResD_IsPhraseDiffers($hResD, $hSourceD) Then

			_ResD_SetValue($hResD, $eRD_sPhrase, _
					($hSourceD[$eSD_aExcel])[$i][$hSourceD[$eSD_iPhrasesCol]])

			_ResD_DictAddRecord($hResD, $eRD_oPhrases, $eRD_sPhrase)

			_ResD_DictReInit($hResD, $eRD_oPhraseUrlsDomains)
		EndIf

		_ResD_SetUrl($hResD, $hSourceD)

		If @error Then ContinueLoop

		_ResD_SetDomain($hResD, True)

		_ResD_CPP_ConsiderDomain($hResD)
	Next
EndFunc   ;==>_ResD_CPP_Core


; учесть домен для фразы (если применимо)

Func _ResD_CPP_ConsiderDomain(ByRef $hResD)

	_ResD_DictAddRecord($hResD, $eRD_oPhraseUrlsDomains, $eRD_sDomain)

	If @error Then Return

	If $hResD[$eRD_oDomains].Exists($hResD[$eRD_sDomain]) Then

		$hResD[$eRD_oPhrases].Item($hResD[$eRD_sPhrase]) = _
				$hResD[$eRD_oPhrases].Item($hResD[$eRD_sPhrase]) + 1
	EndIf
EndFunc   ;==>_ResD_CPP_ConsiderDomain

#EndRegion ; класс выходных данных


#Region ; класс файла результатов

Global Const $eFileResGUID = "ca6e414e-c6f8-4eb7-8551-cce316dd333d"

Global Enum _
		$eFR_iGUID, _            ; GUID
		$eFR_sFilePath, _        ; путь к файлу
		$eFR_iSize               ; размер массива

; конструктор

Func FileRes()

	Local $hResFile[$eFR_iSize]

	$hResFile[$eFR_iGUID] = $eFileResGUID

	Return $hResFile

EndFunc   ;==>FileRes


Func _FileRes_IsObject(ByRef Const $hResFile)

	Return UBound($hResFile) = $eFR_iSize And _
			$hResFile[$eFR_iGUID] == $eFileResGUID

EndFunc   ;==>_FileRes_IsObject


Func _FileRes_SetFilePath(ByRef $hResFile, $sMainPart)

	Local $sFilePathFull, _
			$sFilePathBase = @ScriptDir & "\" & $sMainPart & "_", _
			$sFileNameEnd = ".csv"

	Local $iSuffix = 0

	Do
		$iSuffix += 1
		$sFilePathFull = $sFilePathBase & $iSuffix & $sFileNameEnd

	Until Not FileExists($sFilePathFull)

	$hResFile[$eFR_sFilePath] = $sFilePathFull

EndFunc   ;==>_FileRes_SetFilePath


; для сохранения списка доменов с указанием их встречаемости по фразам

Func FileRes_SaveDomains(ByRef $hResFile, ByRef Const $hResD)

	If Not _FileRes_IsObject($hResFile) _
			Or Not ResD_IsObject($hResD) Then Return

	_FileRes_SetFilePath($hResFile, "domains")

	_FileRes_WriteCSV($hResFile, $hResD, $eRD_aDomains)

EndFunc   ;==>FileRes_SaveDomains


; для сохранения списка фраз и встречаемости заданных доменов по ним

Func FileRes_SavePhrases(ByRef $hResFile, ByRef Const $hResD)

	If Not _FileRes_IsObject($hResFile) _
			Or Not ResD_IsObject($hResD) Then Return

	_FileRes_SetFilePath($hResFile, "phrases")

	_FileRes_WriteCSV($hResFile, $hResD, $eRD_aPhrases)

EndFunc   ;==>FileRes_SavePhrases


Func _FileRes_WriteCSV(ByRef Const $hResFile, ByRef Const $hResD, $iIndex)

	If Not _FileRes_IsObject($hResFile) _
			Or Not ResD_IsObject($hResD) Then Return

	Local $hFileRes = FileOpen($hResFile[$eFR_sFilePath], $FO_OVERWRITE + $FO_UTF8)

	If Not $hFileRes Then _MsgBoxFireExit($MB_ICONERROR, $eMsgBoxTitle, _
			"Не получилось открыть файл " & $hResFile[$eFR_sFilePath] & " для записи")

	; разделитель надо доработать -- чтобы было оборачивание в кавычки; но тогда нужно и каждый элемент массива обработать -- кавычки в начале строки и в конце

	_FileWriteFromArray($hFileRes, $hResD[$iIndex], Default, Default, ";")

	If @error Then _MsgBoxFireExit($MB_ICONERROR, $eMsgBoxTitle, _
			"Не получилось записать данные в файл (ошибка " & @error & ")")

	FileClose($hFileRes)

EndFunc   ;==>_FileRes_WriteCSV


Func FileRes_DisplayEndMess(ByRef Const $hResFile)

	Local $vTime = _DateDiff('n', $eStart, _NowCalc())

	$vTime = "(время работы: ~ " & $vTime & " мин)"

	MsgBox($MB_ICONINFORMATION, $eMsgBoxTitle, "Результат записан в файл:" _
			 & @CRLF & _GetFileNameFromPath($hResFile[$eFR_sFilePath]) _
			 & @CRLF & $vTime)

EndFunc   ;==>FileRes_DisplayEndMess

#EndRegion ; класс файла результатов


#Region ; выполнение

Route()

Func Route()

	Local $hSourceD = SourceD()

	SourceD_SetArrays($hSourceD)

	Local $hResD = ResD()

	Local $hResFile = FileRes()

	If Not IsArray($hSourceD[$eSD_aDomains]) Then

		ResD_CountDomainsPopularity_CDP($hResD, $hSourceD)

		$hSourceD = 0

		FileRes_SaveDomains($hResFile, $hResD)
	Else
		ResD_CountPhrasesPopularity_CPP($hResD, $hSourceD)

		$hSourceD = 0

		FileRes_SavePhrases($hResFile, $hResD)
	EndIf

	$hResD = 0

	FileRes_DisplayEndMess($hResFile)

EndFunc   ;==>Route

#EndRegion ; выполнение
