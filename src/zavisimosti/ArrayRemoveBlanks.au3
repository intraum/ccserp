#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <Array.au3>

#cs
	удаление строк с пустыми элементами из одномерных и двумерных массивов

	Author: https://gitlab.com/intraum

	$aArray 	-- массив;

	$iRowFirst	-- номер первого ряда с данными, по умолчанию работа с массивом со счётчиком;

	$iCol 		-- номер колонки двухмерного массива, в которой искать пустые данные
					(при этом удаляется весь ряд массива с пустой "ячейкой");
					по умолчанию -1, то есть работа с одномерным массивом;

	$bStripWS	-- удалять ли все пробелы из значения перед проверкой на пустоту;
#ce

Func _ArrayRemoveBlanks(ByRef $aArray, $iRowFirst = 1, $iCol = -1, $bStripWS = False)

	__ARBsetDefaults($iRowFirst, $iCol, $bStripWS)
	Local $b2D = $iCol <> -1 ? True : False
	Local $iRowLast = __ARBgetLastRow($aArray, $iRowFirst, $b2D)

	; +2 а не +1 потому что будет добавлен счётчик
	Local $aRowsToDel[$iRowLast + 2]
	Local $iLastRowDel = __ARBconstructMinusArr($aRowsToDel, $aArray, _
			$iRowFirst, $iRowLast, $b2D, $iCol, $bStripWS)

	; если ничего нет на удаление
	If $iLastRowDel = 0 Then Return 0

	; счётчик элементов
	$aRowsToDel[0] = $iLastRowDel
	; для скорости работа велась без изменения размера массива,
	; теперь один раз изменить размер
	ReDim $aRowsToDel[$iLastRowDel + 1]
	Local $vRange = __ARBprepareRange($aRowsToDel)

	Local $iRowUbound
	If $iRowFirst = 1 Then
		; если первый ряд равен одному, то считать, что нужно обновить счётчик
		$iRowUbound = _ArrayDelete($aArray, $vRange)
	Else
		_ArrayDelete($aArray, $vRange)
	EndIf
	If @error Then Return SetError(1)

	If $iRowUbound Then __ARBupdCounter($aArray, $b2D, $iRowUbound - 1)
	Return 1
EndFunc   ;==>_ArrayRemoveBlanks


#Region internal use

Func __ARBsetDefaults(ByRef $iRowFirst, ByRef $iCol, ByRef $bStripWS)
	If $iRowFirst = Default Then $iRowFirst = 1
	If $iCol = Default Then $iCol = -1
	If $bStripWS = Default Then $bStripWS = False
EndFunc   ;==>__ARBsetDefaults


Func __ARBgetLastRow(ByRef $aArray, $iRowFirst, $b2D)
	Local $iRow
	If $iRowFirst = 1 Then
		; если первый ряд равен 1, то считать, что присутствует счётчик в 0-м элементе
		If Not $b2D Then
			$iRow = $aArray[0]
		Else
			$iRow = $aArray[0][0]
		EndIf
	Else
		$iRow = UBound($aArray) - 1
	EndIf
	Return $iRow
EndFunc   ;==>__ARBgetLastRow


; сооружение массива, содержащего номера строк на удаление из основного массива
Func __ARBconstructMinusArr(ByRef $aRowsToDel, ByRef $aMain, _
		$iRowFirst, $iRowLast, $b2D, $iCol, $bStripWS)

	Local $vCellVal
	Local $iRowDel = 0
	For $i = $iRowFirst To $iRowLast
		If Not $b2D Then
			$vCellVal = $aMain[$i]
		Else
			$vCellVal = $aMain[$i][$iCol]
		EndIf

		If $bStripWS Then $vCellVal = StringStripWS($vCellVal, 8)
		If Not $vCellVal Then
			$iRowDel += 1
			$aRowsToDel[$iRowDel] = $i
		EndIf
	Next
	Return $iRowDel
EndFunc   ;==>__ARBconstructMinusArr


; подготовить диапазон строк на удаление в формате _ArrayDelete
Func __ARBprepareRange(ByRef $aRowsToDel)
	Local $vRange
	For $i = 1 To $aRowsToDel[0]
		If $vRange Then
			$vRange &= ';' & $aRowsToDel[$i]
		Else
			$vRange = $aRowsToDel[$i]
		EndIf
	Next
	Return $vRange
EndFunc   ;==>__ARBprepareRange


Func __ARBupdCounter(ByRef $aMain, $b2D, $iDataRows)
	If Not $b2D Then
		$aMain[0] = $iDataRows
	Else
		$aMain[0][0] = $iDataRows
	EndIf
EndFunc   ;==>__ARBupdCounter

#EndRegion internal use
