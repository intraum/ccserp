#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <Excel.au3>


Func _ReadExcelToArr($sPath, $vSheet, ByRef $aExcelData)
	Local $oExcel = _Excel_Open(False, Default, False, False, True)
	If @error Then
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(1)
	EndIf
	Local $oWorkbook = _Excel_BookOpen($oExcel, $sPath, True, False, Default, Default, 0)
	If @error Then
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(2)
	EndIf
	; для некоторых файлов не работает Excel transpose, поэтому последний параметр такой
	$aExcelData = _Excel_RangeRead($oWorkbook, $vSheet, Default, Default, True)
	If @error Or Not IsArray($aExcelData) Then
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(3)
	EndIf
	_Excel_Close($oExcel, False)
EndFunc   ;==>_ReadExcelToArr


; может выдавать ошибку записи эксель на ф-ции _Excel_RangeWrite, и ошибка относится к Transpose;
; причём, ошибка какая-то дурная, непонятно, почему она появляется
Func _WriteExcelFromArray($FilePath, Const ByRef $aArray, $sSheetName)
	Local $iError
	Local $oExcel = _Excel_Open(False, Default, False, False, True)
	If @error Then
		$iError = @error
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(1, $iError)
	EndIf
	Local $oWorkbook
	Local $bFirstWrite = FileExists($FilePath) ? False : True
	If $bFirstWrite Then
		$oWorkbook = _Excel_BookNew($oExcel)
		If @error Then
			$iError = @error
			_ExcelForceCloseNoSave($oExcel)
			Return SetError(2, $iError)
		EndIf
	Else
		$oWorkbook = _Excel_BookOpen($oExcel, $FilePath, Default, Default, Default, Default, 0)
		If @error Then
			$iError = @error
			_ExcelForceCloseNoSave($oExcel)
			Return SetError(3, $iError)
		EndIf
	EndIf

	Local $oSheet = _Excel_SheetAdd($oWorkbook, -1, Default, Default, $sSheetName)
	If @error Then
		$iError = @error
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(4, $iError)
	EndIf

	_Excel_RangeWrite($oWorkbook, $oSheet, $aArray, Default, Default, True)
	If @error Then
		$iError = @error
		_ExcelForceCloseNoSave($oExcel)
		Return SetError(5, $iError)
	EndIf

	If $bFirstWrite Then
		_Excel_BookSaveAs($oWorkbook, $FilePath, Default, True)
		If @error Then
			$iError = @error
			_ExcelForceCloseNoSave($oExcel)
			Return SetError(6, $iError)
		EndIf
	Else
		_Excel_BookSave($oWorkbook)
		If @error Then
			$iError = @error
			_ExcelForceCloseNoSave($oExcel)
			Return SetError(7, $iError)
		EndIf
	EndIf
	_Excel_BookClose($oWorkbook)
	_Excel_Close($oExcel)
EndFunc   ;==>_WriteExcelFromArray


Func _ExcelForceCloseNoSave(ByRef $oExcel)
	_Excel_Close($oExcel, False, True)
EndFunc   ;==>_ExcelForceCloseNoSave
