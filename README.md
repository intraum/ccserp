# Расчёт конкурентов по поисковой выдаче
Описание [здесь](https://seopart.ru/software/ccserp).

Общее описание [здесь](desc_common.md).

Последняя версия для работа с Кеколлектором v3 [здесь](https://gitlab.com/intraum/ccserp/-/tree/v0.2.3).